# XMLtoDataVault

![Example Image](./images/project-logo.png)


## Introduction

This Symfony Console Application is designed to read an XML file and save its content to a database.

## Prerequisites

Before running the application, ensure that you have the following installed:

- [Composer](https://getcomposer.org/) - Dependency manager for PHP
- [Docker](https://www.docker.com/) - Containerization platform

## Getting Started


1. **Install dependencies:**

    ```bash
    composer install
    ```

2. **Start Docker containers:**

    ```bash
    make up
    ```

3. **Apply database migrations:**

    ```bash
    make migrate
    ```
4. **Read feed.xml file and save into database:**

    ```bash
    make data
    ```

## Database

```
   name: app
   Username: admin
   Password: admin
```
## Available Commands

1. **Generate migration file:**

    ```bash
    make migration
    ```
2. **Migrate migration file:**

    ```bash
    make migrate
    ```
3. **Reset database:**

    ```bash
    make reset-db
    ```   
4. **Clear cache:**

    ```bash
    make cache
    ```   
5. **Read xml file and save to database:**

    ```bash
    make data
    ```
6. **Run PHPUnit test:**

    ```bash
    make phpunit
    ```
7. **Start docker container:**

    ```bash
    make up
    ```
8. **Stop docker container:**

    ```bash
    make down
    ```
# Project Architecture
![Example Image](./images/project-architecture.png)

# Database
![Example Image](./images/db.png)

# Error logs
![Example Image](./images/logs.png)