# Makefile

reset-db:
	php bin/console doctrine:schema:drop --full-database --force
	php bin/console doctrine:migrations:migrate --no-interaction

cache:
	php bin/console cache:clear

migrate:
	php bin/console doctrine:migrations:migrate --no-interaction

data:
	php bin/console app:process-feed

phpunit:
	vendor/bin/phpunit

migration:
	php bin/console make:migration --no-interaction

up:
	docker-compose up -d

down:
	docker-compose down