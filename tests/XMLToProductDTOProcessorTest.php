<?php

namespace App\Tests;

use App\Helper\FileFinder;
use App\Parser\XMLToProductDTOProcessor;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class XMLToProductDTOProcessorTest extends KernelTestCase
{
    public function testIsSuccessfulProcessXml()
    {
        self::bootKernel();

        ## find the project directory
        $projectDirectory = self::$kernel->getContainer()->getParameter('kernel.project_dir');

        ## find file path
        $fileFinder = new FileFinder($projectDirectory);
        $filePath = $fileFinder->findFile('feed.xml');

        ## process XML to Product DTOs
        $xmlProcessor = new XMLToProductDTOProcessor();
        $productDTOs = $xmlProcessor->processXml($filePath);


        $this->assertIsArray($productDTOs);

    }

    public function testIsNotSuccessfulProcessXml()
    {
        self::bootKernel();

        ## find the project directory
        $projectDirectory = self::$kernel->getContainer()->getParameter('kernel.project_dir');

        ## find file path
        $fileFinder = new FileFinder($projectDirectory);
        $filePath = $fileFinder->findFile('Mockfeed.xml');

        ## process XML to Product DTOs
        $xmlProcessor = new XMLToProductDTOProcessor();

        $this->expectException(InvalidArgumentException::class);
        $xmlProcessor->processXml($filePath);
    }
}