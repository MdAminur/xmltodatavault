<?php declare(strict_types=1);

namespace App\Handler;

use App\DTO\ProductDTO;
use App\Entity\Product;
use App\Exception\ProductNotFoundException;
use App\Repository\ProductRepository;

class ProductHandler
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function handleProductDTOs(array $productDTOs): void
    {
        $products = array_map(function (ProductDTO $productDTO) {
            return new Product(
                $productDTO->getEntityId(),
                $productDTO->getCategoryName(),
                $productDTO->getSku(),
                $productDTO->getName(),
                $productDTO->getDescription(),
                $productDTO->getShortDescription(),
                $productDTO->getPrice(),
                $productDTO->getLink(),
                $productDTO->getImage(),
                $productDTO->getBrand(),
                $productDTO->getRating(),
                $productDTO->getCaffeineType(),
                $productDTO->getCount(),
                $productDTO->isFlavored(),
                $productDTO->isSeasonal(),
                $productDTO->isInStock(),
                $productDTO->hasFacebook(),
                $productDTO->isIsKCup()
            );
        }, $productDTOs);

        foreach ($products as $product) {
            try {
                $this->productRepository->findProductByEntityId($product->getEntityId());
            }catch (ProductNotFoundException) {
                $this->productRepository->save($product);
            }
        }

    }

}