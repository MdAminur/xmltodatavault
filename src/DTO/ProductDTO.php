<?php declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Attribute\Groups;

class ProductDTO
{
    private int $entityId;
    private ?string $categoryName;
    private ?string $sku;

    private ?string $name;
    private ?string $description;
    private ?string $shortDescription;
    private ?float $price;
    private ?string $link;
    private ?string $image;
    private ?string $brand;
    private ?int $rating;
    private ?string $caffeineType;
    private ?int $count;
    private bool $flavored;
    private bool $seasonal;
    private bool $inStock;
    private bool $facebook;
    private bool $isKCup;

    public function __construct(
        int $entityId,
        ?string $categoryName,
        ?string $sku,
        ?string $name,
        ?string $description,
        ?string $shortDescription,
        ?float $price,
        ?string $link,
        ?string $image,
        ?string $brand,
        ?int $rating,
        ?string $caffeineType,
        ?int $count,
        bool $flavored,
        bool $seasonal,
        bool $inStock,
        bool $facebook,
        bool $isKCup
    )
    {
        $this->entityId = $entityId;
        $this->categoryName = $categoryName;
        $this->sku = $sku;
        $this->name = $name;
        $this->description = $description;
        $this->shortDescription = $shortDescription;
        $this->price = $price;
        $this->link = $link;
        $this->image = $image;
        $this->brand = $brand;
        $this->rating = $rating;
        $this->caffeineType = $caffeineType;
        $this->count = $count;
        $this->flavored = $flavored;
        $this->seasonal = $seasonal;
        $this->inStock = $inStock;
        $this->facebook = $facebook;
        $this->isKCup = $isKCup;
    }


    public function getEntityId(): int
    {
        return $this->entityId;
    }

    public function getCategoryName(): ?string
    {
        return $this->categoryName;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function getCaffeineType(): ?string
    {
        return $this->caffeineType;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function isFlavored(): bool
    {
        return $this->flavored;
    }

    public function isSeasonal(): bool
    {
        return $this->seasonal;
    }

    public function isInStock(): bool
    {
        return $this->inStock;
    }

    public function hasFacebook(): bool
    {
        return $this->facebook;
    }

    public function isIsKCup(): bool
    {
        return $this->isKCup;
    }
}
