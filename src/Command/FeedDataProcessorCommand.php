<?php declare(strict_types=1);

namespace App\Command;

use App\Handler\ProductHandler;
use App\Helper\FileFinder;
use App\Helper\LogoHelper;
use App\Parser\XMLToProductDTOProcessor;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

#[AsCommand(name: 'app:process-feed')]
class FeedDataProcessorCommand extends Command
{
    private FileFinder $fileFinder;

    private ProductHandler $productHandler;

    private LoggerInterface $productImportLogger;

    public function __construct
    (
        FileFinder $fileFinder,
        ProductHandler $productHandler,
        LoggerInterface $productImportLogger
    )
    {
        parent::__construct();
        $this->productHandler = $productHandler;
        $this->fileFinder = $fileFinder;
        $this->productImportLogger = $productImportLogger;
    }

    protected function configure(): void
    {
        $this
            // the command description shown when running "php bin/console list"
            ->setDescription('Read Feed.xml data and save into database')
            // the command help shown when running the command with the "--help" option
            ->setHelp('This command allows you to save xml data into database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        try {
            ini_set('memory_limit', '-1'); # While the current solution solve memory exhaustion issues, it is essential to optimize the database architecture in the future. This involves understanding the relationships among the attributes of the product entity.
            ## Kaufland logo
            $logo = LogoHelper::getKauflandLogo();
            $output->writeln($logo);

            ## processing products
            $output->writeln('<info>Processing products...</info>');
            $feedFilePath = $this->fileFinder->findFile('feed.xml');
            $productDTOs = XMLToProductDTOProcessor::processXml($feedFilePath);
            $this->productHandler->handleProductDTOs($productDTOs);

            $output->writeln('<info>Product saved successfully</info>');

            return Command::SUCCESS;

        } catch (Throwable $e) {
            ## error logging
            $this->productImportLogger->error('Error during importing products into database' . $e->getMessage());
            return Command::FAILURE;
        }
    }
}
