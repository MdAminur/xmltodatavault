<?php declare(strict_types=1);

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\Column(type: "uuid", unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private Uuid $id;

    #[ORM\Column(type: "integer")]
    private int $entityId;
    #[ORM\Column(type: "string", nullable: true)]
    private ?string $categoryName;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $sku;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $name;

    #[ORM\Column(type: "text", nullable: true)]
    private ?string $description;

    #[ORM\Column(type: "text", nullable: true)]
    private ?string $shortDescription;

    #[ORM\Column(type: "float", nullable: true)]
    private ?float $price;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $link;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $image;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $brand;

    #[ORM\Column(type: "integer", nullable: true)]
    private ?int $rating;

    #[ORM\Column(type: "string", nullable: true)]
    private ?string $caffeineType;

    #[ORM\Column(type: "integer", nullable: true)]
    private ?int $count;

    #[ORM\Column(type: "boolean")]
    private bool $flavored;

    #[ORM\Column(type: "boolean")]
    private bool $seasonal;

    #[ORM\Column(type: "boolean")]
    private bool $inStock;

    #[ORM\Column(type: "boolean")]
    private bool $facebook;

    #[ORM\Column(type: "boolean")]
    private bool $isKCup;

    public function __construct(
        int $entityId,
        ?string $categoryName,
        ?string $sku,
        ?string $name,
        ?string $description,
        ?string $shortDescription,
        ?float $price,
        ?string $link,
        ?string $image,
        ?string $brand,
        ?int $rating,
        ?string $caffeineType,
        ?int $count,
        bool $flavored,
        bool $seasonal,
        bool $inStock,
        bool $facebook,
        bool $isKCup
    )
    {
        $this->id = Uuid::v4();
        $this->entityId = $entityId;
        $this->categoryName = $categoryName;
        $this->sku = $sku;
        $this->name = $name;
        $this->description = $description;
        $this->shortDescription = $shortDescription;
        $this->price = $price;
        $this->link = $link;
        $this->image = $image;
        $this->brand = $brand;
        $this->rating = $rating;
        $this->caffeineType = $caffeineType;
        $this->count = $count;
        $this->flavored = $flavored;
        $this->seasonal = $seasonal;
        $this->inStock = $inStock;
        $this->facebook = $facebook;
        $this->isKCup = $isKCup;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getEntityId(): int
    {
        return $this->entityId;
    }

    public function getCategoryName(): ?string
    {
        return $this->categoryName;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function getCaffeineType(): ?string
    {
        return $this->caffeineType;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function isFlavored(): bool
    {
        return $this->flavored;
    }

    public function isSeasonal(): bool
    {
        return $this->seasonal;
    }

    public function isInStock(): bool
    {
        return $this->inStock;
    }

    public function hasFacebook(): bool
    {
        return $this->facebook;
    }

    public function isIsKCup(): bool
    {
        return $this->isKCup;
    }
}

