<?php declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

class ProductNotFoundException extends RuntimeException
{
    public function __construct($message = 'Product not found', $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
