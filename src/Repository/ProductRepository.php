<?php

namespace App\Repository;

use App\Entity\Product;
use App\Exception\ProductNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function save(Product $product)
    {
        $em = $this->getEntityManager();
        $em->persist($product);
        $em->flush();
    }

    public function findProductByEntityId(int $entityId) : Product
    {
        $product = $this->findOneBy(['entityId' => $entityId]);

        if (!$product instanceof Product)
        {
            throw new ProductNotFoundException();
        }

        return $product;
    }

}