<?php declare(strict_types=1);

namespace App\Helper;

use Symfony\Component\Finder\Finder;

class FileFinder
{
    private string $projectDir;
    public function __construct(string $projectDir)
    {
        $this->projectDir = $projectDir;
    }

    public function findFile(string $filename): ?string
    {
        $finder = new Finder();
        $finder->files()->in($this->projectDir)->name($filename);

        foreach ($finder as $file) {
            return $file->getRealPath();
        }

        return null;
    }
}

