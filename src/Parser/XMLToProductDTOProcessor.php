<?php declare(strict_types=1);

namespace App\Parser;

use App\DTO\ProductDTO;
use InvalidArgumentException;

class XMLToProductDTOProcessor
{

    private static function assertFloat(string $value): ?float
    {
        if ($value === '') {
            return null;
        }

        if (is_numeric($value) && str_contains($value, '.') !== true ) {
            throw new InvalidArgumentException("Invalid float value: $value");
        }

        return floatval($value);
    }

    private static function assertInt(string $value): ?int
    {
        if ($value === '') {
            return null;
        }

        if (!preg_match('/^\d+$/', $value)){
            throw new InvalidArgumentException("Invalid integer value: $value");
        }

        return (int)$value;
    }

    private static function assertString(string $value): ?string
    {
        if ($value === '') {
            return null;
        }

        if (is_numeric($value) ||
            $value === "true" ||
            $value === "false" ||
            strtolower($value) === "yes" ||
            strtolower($value) === "no"
        ) {
            throw new InvalidArgumentException("Invalid string value: $value");
        }

        return $value;
    }

    public static function processXml(string $xmlFilePath): array
    {
        $xmlContent = file_get_contents($xmlFilePath);

        $xml = simplexml_load_string($xmlContent);


        $productDTOs = [];

        foreach ($xml->item as $item) {
            $entityId = self::assertInt((string)$item->entity_id);
            $categoryName = self::assertString((string)$item->CategoryName);
            $sku = $item->sku && strlen((string)$item->sku) > 0 ? (string)$item->sku : null;
            $name = self::assertString((string)$item->name);
            $description = self::assertString((string)$item->description);
            $shortDescription = self::assertString((string)$item->shortdesc);
            $price = self::assertFloat((string)$item->price);
            $link = self::assertString((string)$item->link);
            $image = self::assertString((string)$item->image);
            $brand = self::assertString((string)$item->Brand);
            $rating = self::assertInt((string)$item->Rating);
            $caffeineType = self::assertString((string)$item->CaffeineType);
            $count = self::assertInt((string)$item->Count);
            $flavored = (string)$item->Flavored === "Yes";
            $seasonal = (string)$item->Seasonal === "Yes";
            $inStock = (string)$item->Instock === "Yes";
            $facebook = (string)$item->Facebook === "1";
            $isKCup = (string)$item->IsKCup === "1";

            $productDTOs[] = new ProductDTO
            (
                $entityId,
                $categoryName,
                $sku,
                $name,
                $description,
                $shortDescription,
                $price,
                $link,
                $image,
                $brand,
                $rating,
                $caffeineType,
                $count,
                $flavored,
                $seasonal,
                $inStock,
                $facebook,
                $isKCup
            );
        }

        return $productDTOs;
    }
}
